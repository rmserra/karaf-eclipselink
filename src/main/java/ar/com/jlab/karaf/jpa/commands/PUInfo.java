/*
 * Copyright 2015 Rodrigo Serra <rodrigo.serra@jlab.com.ar>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.jlab.karaf.jpa.commands;

import java.io.PrintStream;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.EntityType;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.apache.karaf.shell.table.ShellTable;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author Rodrigo Serra <rodrigo.serra@jlab.com.ar>
 */
@Command(scope = "jpa", name = "info", description = "Show info for persistence unit")
@Service
public class PUInfo extends PUBase {

    @Override
    public Object processEntiyManager(EntityManagerFactory emf, ServiceReference sr) {
        PrintStream out = System.out;
        out.printf("PU \"%s\" bundle %d\n", persistenceUnitName, sr.getBundle().getBundleId());
        out.println("");

        ShellTable tProps = new ShellTable();
        tProps.column("Property");
        tProps.column("Value");
        EntityManager em = emf.createEntityManager();
        try {
            for (Map.Entry<String, Object> entry : em.getProperties().entrySet()) {
                tProps.addRow().addContent(entry.getKey(), entry.getValue());
            }
        } finally {
            em.close();
        }
        tProps.print(out);

        out.printf("\nEntities\n");
        ShellTable tEntities = new ShellTable();
        tEntities.column("Type");
        tEntities.column("Class");
        Set<EntityType<?>> entities = emf.getMetamodel().getEntities();
        for (EntityType entity : entities) {
            tEntities.addRow().addContent(entity.getPersistenceType(), entity.getJavaType().getName());
        }
        tEntities.print(out);
        return null;
    }

}
