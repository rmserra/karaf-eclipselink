/*
 * Copyright 2015 Rodrigo Serra <rodrigo.serra@jlab.com.ar>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.jlab.karaf.jpa.commands;

import java.io.PrintStream;
import java.util.Map;
import java.util.TreeMap;
import javax.persistence.EntityManagerFactory;
import org.apache.karaf.shell.api.action.Action;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.Option;
import org.apache.karaf.shell.api.action.lifecycle.Reference;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.apache.karaf.shell.table.ShellTable;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author Rodrigo Serra <rodrigo.serra@jlab.com.ar>
 */
@Command(scope = "jpa", name = "list", description = "List persistence unit")
@Service
public class PUList implements Action {

    @Reference
    private BundleContext bundleContext;

    @Option(name = "-nf", aliases = "-no-formart")
    boolean noFormat;

    @Override
    public Object execute() throws Exception {
        PrintStream out = System.out;

        ServiceReference[] srvs = bundleContext.getAllServiceReferences(
                EntityManagerFactory.class.getName(),
                "(!(org.apache.aries.jpa.proxy.factory=true))");
        if (srvs != null && srvs.length > 0) {
            Map<String, ServiceReference> pus = new TreeMap<String, ServiceReference>();
            for (ServiceReference srv : srvs) {
                pus.put((String) srv.getProperty("osgi.unit.name"), srv);
            }
            ShellTable table = new ShellTable();
            table.column("Bundle");
            table.column("Persistence unit");
            table.column("Open");
            for (Map.Entry<String, ServiceReference> entry : pus.entrySet()) {
                ServiceReference sr = entry.getValue();
                EntityManagerFactory em = (EntityManagerFactory) bundleContext.getService(sr);
                try {
                    table.addRow().addContent(sr.getBundle().getBundleId(), entry.getKey(), em.isOpen());
                } finally {
                    bundleContext.ungetService(sr);
                }
            }
            table.print(out, !noFormat);
        }
        return null;
    }

}
