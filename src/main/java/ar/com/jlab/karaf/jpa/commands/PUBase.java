/*
 * Copyright 2015 Rodrigo Serra <rodrigo.serra@jlab.com.ar>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.jlab.karaf.jpa.commands;

import java.io.PrintStream;
import javax.persistence.EntityManagerFactory;
import org.apache.karaf.shell.api.action.Action;
import org.apache.karaf.shell.api.action.Argument;
import org.apache.karaf.shell.api.action.lifecycle.Reference;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author Rodrigo Serra <rodrigo.serra@jlab.com.ar>
 */
public abstract class PUBase implements Action {

    @Argument(index = 0, description = "Persistence unit name", required = true)
    protected String persistenceUnitName;

    @Reference
    private BundleContext bundleContext;

    @Override
    public Object execute() throws Exception {
        Object res = null;
        PrintStream out = System.out;
        ServiceReference[] srvs = bundleContext.getAllServiceReferences(
                EntityManagerFactory.class.getName(),
                "(&(!(org.apache.aries.jpa.proxy.factory=true))(osgi.unit.name=" + persistenceUnitName + "))");
        if (srvs != null && srvs.length > 0) {
            ServiceReference sr = srvs[0];
            EntityManagerFactory emf = (EntityManagerFactory) bundleContext.getService(sr);
            try {
                processEntiyManager(emf, sr);
            } finally {
                bundleContext.ungetService(sr);
            }
        } else {
            out.printf("Persistence unit not found.\n");
        }
        return res;
    }

    public abstract Object processEntiyManager(EntityManagerFactory emf, ServiceReference sr);

}
