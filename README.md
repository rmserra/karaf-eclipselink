# Eclipselink 2.6.3 in karaf 4.0.4 #

This project provides necessary features and adapters to use Eclipselink 2.6.3 in Karaf 4.0.5.

### How to compile ###

    git clone https://bitbucket.org/rmserra/karaf-eclipselink.git
    cd karaf-eclipselink
    git checkout rev-1.0.3
    mvn install

### How to use ###

1. Add repo to karaf: feature:repo-add mvn:ar.com.jlab.osgi.karaf/jpa-eclipselink/1.0.3/xml/features
1. install feature: feature:install jpa-eclipselink 
